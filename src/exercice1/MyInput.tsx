import React from 'react';

interface Props {
  value: string;
}

function MyInput(props: Props) {

  const [value, setValue] = React.useState<string>(props.value);

  return (
    <div>
       <input type="text" value={value} onChange={e => setValue(e.target.value)} />
       Current value = {value}
    </div>
  );
}

export default MyInput;
