import React from 'react';
import MyInput from './MyInput';


const initialFormValue = {
    name: ""
};

function Form() {
  const [formData, setFormData] = React.useState(initialFormValue);

  return (
    <div>
      <MyInput value={formData.name} />
      <button onClick={() => setFormData(initialFormValue)}>Reset</button>
    </div>
  );
}

export default Form;
